~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~###**_RESTASSURED_PROJECT_**###~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 #_RestAssured Project  :- In my RestAssured Project ,first we have the two driving mechanism one is "Staic driver class" and second is "Dynamic driver class".

#_Static Driver Class  :- In'static driver class' we are statically calling the test script into main method and executing them , which is not advisible for that very reason we have developed a new method called 'Dynamic driver class'

#_Dynamic Driver Class :- In which we can fetch the test cases to be executed from users from an Excel file & dynamically call the corresponding test script on runtime, This is being done by using "Java.lang.reflect".

#_Then we have "Common Functions" .In common functions we have two categories first is "API Related Common Function" and second is "Utilities". 

#_API Related Common Function :- In this we have the common functions which we are used to trigger the API ,extract the responseBody and extract the response status code.

#_Utilities    :- In this we have created utilities to create a directory when it does not exist  for test log files and if at log directory exist we delete it and then recreate. Then we have another utility between which will automatically take the end point requestBody responseBody in the response headers and add it into a notepad file for each single test script. Then we have a more utility which will read the values from an Excel file.

#_Data/Variable File  :- Then we are using Excel file to input the data to our framework or the request body paramaters are entered into an Excel file.

#_Reports     :-In this project we are using  Allure reports and Extent reports.

#_Libraries   :- The libraries which we are used are as follow :- RestAssured , Apache-Poi, TestNG libraries and dependency management of libraries these are being handle by maven repository.


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~###_***********************_###~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
