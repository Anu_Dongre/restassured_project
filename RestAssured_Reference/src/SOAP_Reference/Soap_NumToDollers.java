package SOAP_Reference;

import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;
import static io.restassured.RestAssured.given;
import org.testng.Assert;
  
public class Soap_NumToDollers {
	public static void main(String[] args) {
//Step1 :- Declare the Base URL.
		String BaseURI = "https://www.dataaccess.com";
//Step2 :- Declare the Request Body
		String RequestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
				+ "   <soapenv:Header/>\r\n" + "   <soapenv:Body>\r\n" + "      <web:NumberToDollars>\r\n"
				+ "         <web:dNum>44.14</web:dNum>\r\n" + "      </web:NumberToDollars>\r\n"
				+ "   </soapenv:Body>\r\n" + "</soapenv:Envelope>";
//Trigger the API & fetch the Responsebody
		// RestAssured.baseURI = BaseURI;
		String ResponseBody = given().header("Content-Type", "text/xml; charset=utf-8").body(RequestBody).when()
				.post("https://www.dataaccess.com/webservicesserver/NumberConversion.wso").then().extract().response()
				.getBody().asString();
//Step4 :- Print the Response body
		System.out.println(ResponseBody);
//Step5 :- Extract the Responsebody Parameter
		XmlPath Xml_res = new XmlPath(ResponseBody);
		String res_tag = Xml_res.getString("NumberToDollarsResult");
		System.out.println(res_tag);
//Step6 :- Validate the Responsebody
		Assert.assertEquals(res_tag, "forty four dollars and fourteen cents" );

	}

}