package API_Common_Methods;

import static io.restassured.RestAssured.given;

public class Common_Method_Handle_API 

{
  public static int post_statusCode(String requestBody, String endpoint)
	{
		int statusCode = given().header("Content-Type", "application/json").body(requestBody).when().post(endpoint)
	    .then().extract().statusCode();
		return statusCode;
   	}
	
	public static String post_responseBody(String requestBody, String endpoint) 
	{
		String responseBody = given().header("Content-Type", "application/json").body(requestBody).when().post(endpoint)
		.then().extract().response().asString();
		return responseBody;

	}
}

